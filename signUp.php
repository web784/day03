<?php
    echo "<!DOCTYPE html> 
        <html lang='vn'> 
        <head>
            <meta charset='UTF-8'>
            <meta name='viewport' content='width=device-width, initial-scale=1'>
            <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css'>
            <link rel='stylesheet' href='signup.css'>
        </head>
        <title>Sign Up</title>
        <body>\n";

    echo "<center>
          <fieldset>
               <form method='POST'>
                    <table style='border-collapse:separate; border-spacing:15px 15px;'>
                    <tr height='32px'>
                        <td class='selectGroup'>
                            <label class='col1'>Họ và tên</label>
                        </td>
                        <td>
                            <input type='text' class='name'>
                         </td>
                    </tr>
                    <tr height='32px'>
                        <td class='selectGroup'>
                            <label class='col1'>Giới tính</label>
                        </td>
                        <td width=30%>";

    $gender = array(0, 1);
    for( $x=0; $x<=1; $x++) {
        if($x == 0){
            echo "<label for='male' class='gender'>
                <input type='radio' class='gender' name='gender'> Nam 
            </label>";
        }
        elseif($x == 1){
            echo "<label for='female' class='gender'>
                <input type='radio' class='gender' name='gender'> Nữ 
            </label>";
        }
    }

    echo "</td>
    </tr>
    <tr height='32px'>
        <td class='selectGroup'>
            <label class='col1'>Phân khoa</label>
        </td>
        <td width=30%>
            <select>
                <option></option>";
     
    $falcuty = array("MAT", "KDL");
    foreach($falcuty as $f){
        if($f == "MAT"){
            echo "<option value='MAT'>
                Khoa học máy tính
            </option>";
        }
        elseif($f == "KDL"){
            echo "<option value='KDL'>
                Khoa học vật liệu
            </option>";
        }
    }
    echo "</select>
    </td>
</tr>
</table>
<input type='submit' value='Đăng ký' id='submit'/>
</form>
</fieldset>
</center>
</body>
</html>";
?>
